package lab01;
import java.util.Scanner;

public class lab007 {
	 public static void main(String[] args) {
		 			Q1();
		 			System.out.println(" ");
		 			Q2();

	    }
	 public static void Q1() {
		 int counter = 0;
		   do {
		     System.out.println("Counter :" + counter);
		     counter++;
		   } while (counter <= 20);
		 
	 }
	 public static void Q2() {
		 Scanner readern  = new Scanner(System.in);
	        int number;

	        System.out.println("Determine odd/even program");

	        do {
	            System.out.print("Enter odd number to exit loop: ");
	            number = readern.nextInt();

	            if (number % 2 == 0) {
	                System.out.println("You entered " + number + ", it's even.");
	            } else {
	                System.out.println("You entered " + number + ", it's odd.");
	            }

	        } while (number % 2 == 0);

	        System.out.println("Exited loop.");
	 }

}