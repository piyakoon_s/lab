package lab01;

public class lab009 {
	public static void main(String args[]) {
        for (int count = 1; count <= 20; count++) {
            if (count == 11) // ถ้า count มีค่าเป็น 11 คำสั่ง continue ภายใน if จะทำงาน
                continue; // เริ่มต้นรอบใหม่โดยไม่สนใจคำสั่งที่เหลือด้านล่าง
                System.out.printf("%d ", count); // แสดงค่าตัวแปร count 
        }
        System.out.println("\nUsed continue to skip printing 11");
    }

}